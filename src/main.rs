use colorful::{Color, Colorful};
use libcli::args::*;
const VERSION: &'static str = "0.1.0";

fn main() {
    let specs = [
        OptionSpec::new(
            '\0',
            "(unnamed)",
            "Files and directories to watch for changes (default '.')",
            false,
            OptionPolicy::AtLeast(0),
        ),
        OptionSpec::new('\0', "", "Command to run", true, OptionPolicy::AtLeast(0)),
        OptionSpec::new(
            'k',
            "kill",
            "Kill subprocess instead of waiting for it when rerunning",
            false,
            OptionPolicy::Exact(0),
        ),
        OptionSpec::new(
            'c',
            "clear",
            "Clear the screen before each rerun",
            false,
            OptionPolicy::Exact(0),
        ),
        OptionSpec::new(
            'w',
            "wait",
            "Wait for change before running command, command is normally run at startup",
            false,
            OptionPolicy::Exact(1),
        ),
        OptionSpec::new(
            'i',
            "interval",
            "Interval in millis to check for changes (default 100)",
            false,
            OptionPolicy::Exact(1),
        ),
        OptionSpec::new(
            'h',
            "help",
            "Display usage screen",
            false,
            OptionPolicy::FinalizeIgnore(),
        ),
        OptionSpec::new(
            'v',
            "version",
            "Print version and exit",
            false,
            OptionPolicy::FinalizeIgnore(),
        ),
    ];

    let config = Config::new_env(&specs).unwrap_or_else(|err| {
        println!(
            "{} {}\nUse {} for usage",
            "Error:".color(Color::Red).bold(),
            err,
            "--help".color(Color::Green)
        );
        std::process::exit(1);
    });

    if config.option("help").is_some() {
        let description = "Rerun executes on one or more directories and files and reruns a command if any file changes.\nThe files/directories to watch are specified first, then eventual options\nThe command is specified after '--'\nExample:\n    rerun src tests -k -- cargo test\nAn argument of '{}' will be subsituted for the changed files";

        println!(
            "Rerun\n\n{}\n{}",
            Config::generate_usage(&specs, true, true),
            description
        );
        std::process::exit(0);
    }

    if config.option("version").is_some() {
        println!("version: {}", VERSION);
        std::process::exit(0);
    }

    rerun::run(config)
}
