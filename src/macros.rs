#[macro_use]
macro_rules! error {
    ($($arg:tt)*) => ({
        eprintln!($($arg)*);
        return Default::default();
    })
}
