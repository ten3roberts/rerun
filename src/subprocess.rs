use std::process;

pub struct Subprocess {
    process: process::Child,
}

impl Subprocess {
    /// Starts a new process
    pub fn new(command: &[String]) -> Option<Subprocess> {
        let process = match process::Command::new(&command[0])
            .args(&command[1..])
            .spawn()
        {
            Ok(v) => v,
            Err(e) => error!("Error: {:?}", e),
        };

        Some(Subprocess { process })
    }

    /// Sends a kill signal and waits for process to exit
    pub fn wait_kill(&mut self) -> Option<i32> {
        // Process is already done
        match self.process.try_wait() {
            Ok(Some(status)) => {
                println!("Process finished");
                return status.code();
            }
            Ok(None) => {}
            Err(e) => error!("Failed to try_wait for child process '{}'", e),
        }

        match self.process.kill() {
            Ok(_) => None,
            Err(e) => error!("Failed to kill child process '{}'", e),
        }
    }

    /// Wait for process to exit normally
    pub fn wait(&mut self) -> Option<i32> {
        match self.process.wait() {
            Ok(status) => status.code(),
            Err(e) => error!("Failed to wait on child process '{}'", e),
        }
    }

    /// Returns true if process is still running
    pub fn is_running(&mut self) -> bool {
        match self.process.try_wait() {
            Ok(Some(_)) => false,
            Ok(None) => true,
            Err(e) => error!("Failed to get status of child process '{}'", e),
        }
    }
}
