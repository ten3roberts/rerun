# Rerun
Rerun is a simple command line utility for automatically rerunning a command when files change

Rerun takes the first positional arguments as files and directories to watch for changes
It will then run the command specified after '--'

Other options should be specified after files but before command as all arguments after command are passed to the command, not rerun

If files is left empty, it defaults to the current directory

' {} ' will be replace by the changed files in command

## Examples
`rerun src -- cargo run` Execute cargo run when any file in src/ changes
`rerun . -- echo {}` Print all changed files in the current directory
